#!/bin/bash

# CIAO Mondo main script
# Juan Caballero <juan.caballero@databiology.com>
# (C) 2019 Databiology

# Global variables definition
SCRATCH=/scratch
RESULTSDIR="$SCRATCH/results"
INPUTDIR="$SCRATCH/input"
OUTFILE="$RESULTSDIR/ciao_mondo_out.txt"

# Collecting app parameters from /scratch/parameters.json
MESSAGE=$(jq -r '.["APPLICATION"] .message' "$SCRATCH/parameters.json")
KEEPRUN=$(jq -r '.["APPLICATION"] .keep_run' "$SCRATCH/parameters.json")
TASK=$(jq -r '.["APPLICATION"] .task' "$SCRATCH/parameters.json")

# creating the initial output
echo "CIAO Mondo: $MESSAGE" > $OUTFILE

# Perform task
case $TASK in
    check_time)
        echo "Our time is $(date)" >> "$OUTFILE"
    ;;
    check_files)
        if [ -e "$SCRATCH/projections.json" ]
        then
            for FILE in $(jq -r '.[].relativePath' "$SCRATCH/projections.json")
            do
                if [ -e "$INPUTDIR/$FILE" ]
                then
                    ls -l "$INPUTDIR/$FILE" >> "$OUTFILE"
                else
                    echo "FILE $INPUTDIR/$FILE is missing" >> "$OUTFILE"
                fi
            done
        else
            echo "No projection was found, please add one which project resources relativePath to the app, but here are the files under $INPUTDIR" >> "$OUTFILE"
            ls -Rl "$INPUTDIR" >> "$OUTFILE"
        fi
    ;;
    *)
        echo "No task to do" >> "$OUTFILE"
    ;;
esac

# Keep running mode
if [[ $KEEPRUN == "true" ]]
then
    sleep 3600
fi

